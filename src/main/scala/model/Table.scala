package model

case class Slot(pos: Int, stone: Stone.Value)

object Table {
  val MaxPos = 6

  def emptySlots = (0 to 41) map (Slot(_, Stone.Empty))
}

class Table(var slots: Seq[Slot] = Table.emptySlots,
            var value: Int = Int.MinValue,
            var beta: Int = Int.MaxValue,
            var alpha: Int = Int.MinValue,
            var isMax: Boolean = true,
            var parent: Option[Table] = None) {

  def reset = slots = Table.emptySlots

  def setPos(pos: Int, player: Stone.Value) = slots = slots.updated(pos, Slot(pos, player))

  def getFirstPossible(pos: Int) = {
    slots.zipWithIndex.collect {
      case (slot, i) if i % 7 == pos => slot
    }.reverse.find { slot => slot.stone == Stone.Empty }
  }

  def haveWinner(player: Stone.Value) = {
    verifyWin(diagonals ++ columns ++ lines, player)
  }

  def verifyWin(groups: Iterable[Seq[Slot]], player: Stone.Value) = {
    groups.exists { group =>
      var count = 0
      group.exists { slot =>
        if (slot.stone == player)
          count += 1
        else
          count = 0
        count == 4
      }
    }
  }

  def getNextChild(pos: Int = 0): Option[Table] = {
    getFirstPossible(pos) match {
      case Some(slot) => Some(generateChild(slot.pos))
      case None => if (pos < Table.MaxPos) getNextChild(pos + 1) else None
    }
  }

  def generateChild(pos: Int) = {
    new Table(slots.updated(pos, Slot(pos, player)), value, beta, alpha, !isMax, Some(this))
  }


  def player = if (isMax) Stone.IA else Stone.Player

  def columns = slots.groupBy(mod).values

  def lines = slots.groupBy(div).values

  def diagonals = (slots.groupBy(modPlusDiv).values ++ slots.groupBy(modMinusDiv).values).filterNot(_.size < 4)

  def mod(slot: Slot) = slot.pos % 7

  def div(slot: Slot) = slot.pos / 7

  def modPlusDiv(slot: Slot) = mod(slot) + div(slot)

  def modMinusDiv(slot: Slot) = mod(slot) - div(slot)
}
