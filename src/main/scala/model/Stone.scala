package model

object Stone extends Enumeration {
  type Stone = Value
  val Player, IA, Empty = Value
}