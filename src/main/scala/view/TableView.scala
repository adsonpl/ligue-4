package view

import model.{Slot, Stone}
import controller.TableController

import scalafx.Includes._
import scalafx.application.JFXApp
import scalafx.scene.Scene
import scalafx.scene.paint.Color._
import scalafx.scene.shape.{Circle, Rectangle}

object TableView extends JFXApp {

  var invalidPos: List[Int] = List()

  stage = new JFXApp.PrimaryStage {
    title.value = "Ligue 4"
    width = 405
    height = 390
  }

  TableController.repaint

  def startScene(slots: Seq[Slot]) {
    stage.scene = new Scene {
      fill = Gray
      content = rectangles ++ circles(slots)
    }
  }

  def invalidate(pos: Int) = invalidPos = pos :: invalidPos

  def rectangles = (0 to 6).map(createRectangle)

  def circles(slots: Seq[Slot]) = slots.map(createCircle)

  def createRectangle(pos: Int) = new Rectangle {
    x = 30 + getX(pos)
    y = 10 + getY(pos)
    width = 40
    height = 40
    if (invalidPos.contains(pos)) {
      fill = Black
    } else {
      fill <== when(hover) choose Green otherwise Red
      onMouseClicked = () => TableController.click(pos)
    }
  }

  def createCircle(slot: Slot) = new Circle {
    centerX = 50 + getX(slot.pos)
    centerY = 80 + getY(slot.pos)
    radius = 20
    fill = getColor(slot.stone)
  }

  def getColor(stone: Stone.Value) = stone match {
    case Stone.Player => Blue
    case Stone.IA => Red
    case Stone.Empty => Yellow
  }

  def getX(pos: Int) = 50 * (pos % 7)

  def getY(pos: Int) = 50 * (pos / 7)
}