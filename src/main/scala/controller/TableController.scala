package controller

import model.{Stone, Table}
import view.TableView

import scala.util.Random

object TableController {
  var currentTable = new Table

  var currentPlayer = Stone.Player

  def click(pos: Int) {
    applyPos(pos)
    if (currentTable.haveWinner(currentPlayer)) println("win!")
    changePlayer
//    currentTable = currentTable.getNextChild(currentPlayer).get
//    changePlayer
    repaint
//    nextMove
  }

  def nextMove: Unit ={
    currentTable.getNextChild() match {
      case Some(table) =>
        currentTable = table
        TableView.startScene(currentTable.slots)
        changePlayer
        nextMove
      case None =>
    }
  }

  def random = Random.shuffle(0 to 6).toSeq.head

  def applyPos(pos: Int): Unit = currentTable.getFirstPossible(pos) match {
    case Some(slot) =>
      if (slot.pos / 7 == 0) invalidate(pos)
      currentTable.setPos(slot.pos, currentPlayer)
    case _ =>
  }

  def invalidate(pos: Int) = TableView.invalidate(pos)

  def changePlayer = currentPlayer = if (currentPlayer == Stone.Player) Stone.IA else Stone.Player


  def repaint = TableView.startScene(currentTable.slots)


}
